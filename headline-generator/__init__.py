import logging
from random import randint

import azure.functions as func

HEADLINES = [
    "Liverpool mutation threat as 32 worrying new cases found - also discovered in Bristol",
    "Captain Sir Tom Moore dies aged 100 after testing positive for Covid-19",
    "Freezing January the coldest in a decade",
    "Banker loses out in £600,000 legal battle with ex-wife",
    "Number of deaths in second wave of COVID infections overtakes first wave",
    "EU warns of consequences if vaccine orders not delivered",
    "Man and five small children shot dead at US home",
    "Judge refuses to halt eviction of HS2 activists from Euston tunnel",
    "'Buy now, pay later' firms such as Klarna to face FCA regulation",
    "Average British family spent extra £50 on groceries in January"
]


def main(req: func.HttpRequest) -> func.HttpResponse:
    headline = HEADLINES[randint(0, (len(HEADLINES) - 1))]
    return func.HttpResponse(
        body=headline
    )
