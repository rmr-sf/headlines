import logging
import uuid
from os import environ

import azure.functions as func
from azure.cosmos import CosmosClient
from requests.adapters import HTTPAdapter
import requests


def main(mytimer: func.TimerRequest) -> None:
    container_client = connect_db(
        cosmos_url=environ['COSMOS_ENDPOINT'],
        cosmos_key=environ['COSMOS_KEY'],
        db_name=environ['DB_NAME'],
        container_name=environ['COSMOS_CONTAINER']
    )

    http = get_http_session()
    response = http.get(environ['API_ENDPOINT'], timeout=5)
    
    container_client.upsert_item({
        'id': str(uuid.uuid4()),
        'text': response.text
    })


def connect_db(cosmos_url, cosmos_key, db_name, container_name):
    cosmos_client = CosmosClient(
        url=cosmos_url,
        credential=cosmos_key
    )
    db_client = cosmos_client.get_database_client(db_name)
    
    return db_client.get_container_client(container_name)


def get_http_session():
    adapter = HTTPAdapter(max_retries=2)
    retry_session = requests.Session()
    retry_session.mount("https://", adapter)
    retry_session.mount("http://", adapter)

    return retry_session
