[![Gitpod ready-to-code](https://img.shields.io/badge/Gitpod-ready--to--code-blue?logo=gitpod)](https://gitpod.io/#https://gitlab.com/rmr-sf/headlines)

# News Headline Scraper Demo

This pair of Azure Functions serve as a demonstration for deployment to Azure.

The first function `headline-generator` will return a random headline taken from various UK news outlets when it receives a GET request.

The second function `news-scraper` will call the API of the first function on a schedule and insert the response into Cosmos DB.
