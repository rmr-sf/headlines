FROM gitpod/workspace-full

# Install Azure Function Core Tools
RUN npm i -g azure-functions-core-tools@3 --unsafe-perm true

# Install Azure Storage emulator
RUN npm i -g azurite

# Install Azure CLI
RUN curl -sL https://aka.ms/InstallAzureCLIDeb | sudo bash
